#!/usr/bin/env bash

## Update packages
apt-get update
apt-get -y install python-software-properties
apt-add-repository ppa:ondrej/php5-oldstable
apt-get update

## MySQL
export DEBIAN_FRONTEND=noninteractive
apt-get -q -y install mysql-server mysql-client
mysqladmin -u root password vagrant
/etc/init.d/mysql restart

## Apache
apt-get -y install apache2

## PHP5
apt-get -y install php5 libapache2-mod-php5

## PHP Modules
apt-get -y install php5-mysql php5-curl php5-gd php5-intl php-pear php5-imagick php5-imap php5-mcrypt php5-memcache php5-ming php5-ps php5-pspell php5-recode php5-snmp php5-sqlite php5-tidy php5-xmlrpc php5-xsl

## Run Apache as Vagrant user/group
cat << EOF >> /etc/apache2/httpd.conf
User vagrant
Group vagrant
EOF

## PHPMyAdmin
apt-get -y install phpmyadmin
echo "Include /etc/phpmyadmin/apache.conf" >> /etc/apache2/apache2.conf

## Symlinks
rm -rf /var/www
ln -fs /vagrant /var/www
a2enmod rewrite
sed -i '/AllowOverride None/c AllowOverride All' /etc/apache2/sites-available/default

## Restart Apache
/etc/init.d/apache2 restart

## CURL
apt-get install -y curl

## UNZIP
apt-get -y install unzip
 
## Concrete5 563
##curl -LO https://github.com/concrete5/concrete5/archive/master.zip
curl -Lo concrete.zip https://www.concrete5.org/download_file/-/view/75930/8497
unzip concrete.zip
mv concrete5.6.3.3/* /var/www

## Create database
mysql -u root -pvagrant -e "create database concrete56";

## Install concrete5
## php concrete5-master/cli/install-concrete5.php --db-server=localhost --db-username=root --db-password=vagrant --db-database=concrete5 --admin-password=iwm@dm1n --admin-email=example@example.com --starting-point=standard --target=/var/www

## Restart Apache
/etc/init.d/apache2 restart

## Add a cron backup task?