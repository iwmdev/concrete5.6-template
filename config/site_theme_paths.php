<?php 
defined('C5_EXECUTE') or die("Access Denied.");
$theme = 'themename';
$v = View::getInstance();

// TODO - make this honor * better, actually work for more than just dashboard
// To change page design change "VIEW_CORE_THEME" to the name of the theme folder

$v->setThemeByPath('/dashboard', 'dashboard');
$v->setThemeByPath('/dashboard/*', 'dashboard');

$v->setThemeByPath('/page_forbidden', $theme);
$v->setThemeByPath('/page_not_found', $theme);
$v->setThemeByPath('/install', $theme);
$v->setThemeByPath('/login', $theme);
$v->setThemeByPath('/register', $theme);
$v->setThemeByPath('/maintenance_mode', $theme);