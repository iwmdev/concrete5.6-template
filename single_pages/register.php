<?php  defined('C5_EXECUTE') or die("Access Denied."); ?>
<div class="row">
	<div class="col-sm-12">
		<span class="headerBorder clearfix"><h1><?php echo t('Site Registration')?></h1></span>
		<?php Loader::element('system_errors', array('error' => $error)); ?>
	</div>
</div>

<div class="ccm-form">

<?php  
$attribs = UserAttributeKey::getRegistrationList();

if($success) { ?>
<div class="row">
	<div class="register-msg col-sm-6 col-sm-offset-3 well">
		<h4>Success!</h4>
		<?php
			switch($success) { 
				case "registered": 
					?>
					<p><strong><?php echo $successMsg ?></strong><br/><br/>
					<a href="<?php echo $this->url('/')?>" class="btn red-button"><?php echo t('Return to Home')?></a></p>
					<?php  
				break;
				case "validate": 
					?>
					<p><?php echo $successMsg[0] ?></p>
					<p><?php echo $successMsg[1] ?></p>
					<p><a href="<?php echo $this->url('/')?>" class="btn red-button"><?php echo t('Return to Home')?></a></p>
					<?php 
				break;
				case "pending":
					?>
					<p><?php echo $successMsg ?></p>
					<p><a href="<?php echo $this->url('/')?>" class="btn red-button"><?php echo t('Return to Home')?></a></p>
		            <?php 
				break;
			}
		?>
	</div>
</div>
<?php  
} else { ?>

<form method="post" action="<?php echo $this->url('/register', 'do_register')?>" class="form-horizontal">
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2">
			<fieldset>
				<legend><?php echo t('Your Details')?></legend>
				<?php  if ($displayUserName) { ?>
						<div class="control-group">
						<?php echo  $form->label('uName',t('Username')); ?>
						<div class="controls">
							<?php echo  $form->text('uName','',array('class'=>'form-control')); ?>
						</div>
					</div>
				<?php  } ?>
			
				<div class="control-group">
					<?php  echo $form->label('uEmail',t('Email Address')); ?>
					<div class="controls">
						<?php  echo $form->email('uEmail','',array('class'=>'form-control')); ?>
					</div>
				</div>
				<div class="control-group">
					<?php  echo $form->label('uPassword',t('Password')); ?>
					<div class="controls">
						<?php  echo $form->password('uPassword','',array('class'=>'form-control')); ?>
					</div>
				</div>
				<div class="control-group">
					<?php  echo $form->label('uPasswordConfirm',t('Confirm Password')); ?>
					<div class="controls">
						<?php  echo $form->password('uPasswordConfirm','',array('class'=>'form-control')); ?>
					</div>
				</div>
			<?php  if (count($attribs) > 0) { ?>
				<?php 
					$af = Loader::helper('form/attribute');
					foreach($attribs as $ak) { 
				?> 
					<?php echo  $af->display($ak, $ak->isAttributeKeyRequiredOnRegister());	?>
				<?php
					}
				?>
			<?php  } ?>
			</fieldset>
			<div class="captcha">
				<?php  if (ENABLE_REGISTRATION_CAPTCHA) { ?>
				
					<div class="control-group">
						<?php  $captcha = Loader::helper('validation/captcha'); ?>			
						<?php echo $captcha->label()?>
						<div class="controls">
						<?php 
					  		$captcha->showInput(); 
							$captcha->display();
					  	?>
						</div>
					</div>
				<?php  } ?>
			</div>
			<hr />
			<div class="actions">
				<?php echo $form->hidden('rcID', $rcID); ?>
				<?php echo $form->submit('register', t('Register') . ' &gt;', array('class' => 'gold-button'))?>
			</div>
		</div>
	</div>
</form>
<?php  } ?>

</div>