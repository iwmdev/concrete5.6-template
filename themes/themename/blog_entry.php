<?php
    defined('C5_EXECUTE') or die(_("Access Denied."));
    $this->inc('elements/header.php');
?>
            <section class="mainContent col-sm-8">
                <h1><?php echo $c->getCollectionName(); ?></h1> 
                <span class="metadata">
                    <?php 
                        $u = new User();
                        if ($u->isRegistered()) { ?>
                            <?php  
                            if (Config::get("ENABLE_USER_PROFILES")) {
                                $userName = '<a href="' . $this->url('/profile') . '">' . $u->getUserName() . '</a>';
                            } else {
                                $userName = $u->getUserName();
                            }
                        }
                        echo 'Posted by: <span class="post-author">' . $userName . ' on ' . $c->getCollectionDatePublic('F jS, Y') . '</a></span>';
                    ?>
                </span>
                <?php 
                    $a = new Area('Main');
                    $a->display($c);
                ?>
                <div id="main-content-post-footer-share">
                    <p>Share:
                        <a href="mailto:?subject=<?php  echo $c->getCollectionName(); ?>&body=<?php  echo $nav->getLinkToCollection($c, true); ?>"><img class="main-content-post-footer-share-email" src="<?php  echo $this->getThemePath(); ?>/images/icon_email.png" alt="Email" /></a>
                        <a href="https://twitter.com/share"><img class="main-content-post-footer-share-twitter" src="<?php  echo $this->getThemePath(); ?>/images/icon_twitter.png" alt="Share on Twitter" /></a>
                        <a href="http://www.facebook.com/share.php?u=<?php  echo $nav->getLinkToCollection($c, true); ?>"><img class="main-content-post-footer-share-facebook" src="<?php  echo $this->getThemePath(); ?>/images/icon_facebook.png" alt="Share on Facebook" /></a>
                    </p>
                </div>
            </section>
            <aside class="sidebar col-sm-4">
                <?php
                    $this->inc('elements/sidebar.php');
                ?>
            </aside>
<?php
    $this->inc('elements/footer.php');
?>