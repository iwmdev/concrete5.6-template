<?php
    defined('C5_EXECUTE') or die(_("Access Denied."));
    $this->inc('elements/header.php');
?>
            <section class="mainContent col-sm-12">
                <?php
                    $main = new Area('Main');
                    $main->display($c);
                ?>
            </section>
<?php
    $this->inc('elements/footer.php');
?>